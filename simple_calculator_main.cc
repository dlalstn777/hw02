// simple_calculator_main.cc
#include "simple_calculator.h"
#include<stdio.h>
#include<iostream>
using namespace std;
int main(){
	while(1){
		char op;
		double Fnum,num;
		cin>>Fnum;	
		SimpleCalculator a(Fnum);
		while(1){
			cin>>op;
			if(op=='=') break;
			else if(op!='+'&&op!='-'&&op!='*'&&op!='/') return 0;
			cin>>num;
			if(op=='+') a.Add(num);	
			else if(op=='-') a.Subtract(num);
			else if(op=='*') a.Multiply(num);
			else if(op=='/') a.Divide(num);	
		}	
		cout<<a.value()<<'\n';	
	}	
}
