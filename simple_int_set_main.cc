// simple_int_set_main.cc
#include"simple_int_set.h"
#include<iostream>
#include<stdlib.h>
using namespace std;
int main(){
	while(1){
	SimpleIntSet a,b,c;
	int *Farr,*Sarr;
	string str;
	char op;
	int i=0,h=0,f;
	Farr =new int[MAX_SIZE];
	Sarr =new int[MAX_SIZE];
	cin>>str;
	if(str!="{") return 0;
	while(1){
		cin>>str;
		if(str=="}") break;
		Farr[i]=atoi(str.c_str()),i++;
	}
	a.Set(Farr,i);
	cin>>str;
	if(str=="+") op='+';
	if(str=="-") op='-';
	if(str=="*") op='*';
	cin>>str;
	if(str!="{") return 0;
	while(1){
		cin>>str;
		if(str=="}") break;
		Sarr[h]=atoi(str.c_str()),h++;	
	}
	b.Set(Sarr,h);

	if(op=='+') c=a.Union(b);
	if(op=='-') c=a.Difference(b);
	if(op=='*') c=a.Intersect(b);
	delete[]Farr;
	delete[]Sarr;
	cout<<'{'<<' ';
	for(i=0;i<c.size();i++) cout<<c.values()[i]<<' ';
	cout<<'}'<<'\n';
	}
}
