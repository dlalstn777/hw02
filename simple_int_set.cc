// simple_int_set.cc
#include"simple_int_set.h"
#include<stdio.h>
int* Sort(int* values,int size){
	int i,f,val;
	for(i=0;i<size;i++){
		for(f=1;f<size;f++){
			if(values[f]<values[f-1]) val=values[f],values[f]=values[f-1],values[f-1]=val;
		}
	}
	return values;
}
SimpleIntSet SimpleIntSet::Intersect(const SimpleIntSet& int_set) const{
	SimpleIntSet who;
	int ii,hh=0,ff;
	int *arr;
	arr=new int[MAX_SIZE];
	for(ii=0;ii<size_;ii++){
		for(ff=0;ff<int_set.size();ff++){
			if(values_[ii]==int_set.values()[ff]) arr[hh]=int_set.values()[ff],hh++;
		} 
	}
	Sort(arr,hh);
	who.Set(arr,hh);
	delete[]arr;
	return who;
}
SimpleIntSet SimpleIntSet::Union(const SimpleIntSet& int_set) const{
	SimpleIntSet who;
	int ii,hh=0,ff,a;
	int *arr;
	arr=new int[MAX_SIZE];
	for(ii=0;ii<size_;ii++) arr[hh]=values_[ii],hh++;
	for(ff=0;ff<int_set.size();ff++){
		a=0;
		for(ii=0;ii<size_;ii++){
			if(values_[ii]!=int_set.values()[ff]) a++;
		}
		if(a==size_) arr[hh]=int_set.values()[ff],hh++; 
	}
	Sort(arr,hh);
	who.Set(arr,hh);
	delete[]arr;
	return who;
}
SimpleIntSet SimpleIntSet::Difference(const SimpleIntSet& int_set) const{
	SimpleIntSet who;
	int ii,hh=0,ff,a;
	int *arr;
	arr=new int[MAX_SIZE];
	for(ii=0;ii<size_;ii++){
		a=0;
		for(ff=0;ff<int_set.size();ff++){
			if(values_[ii]!=int_set.values()[ff]) a++;
		} 
		if(a==int_set.size()) arr[hh]=values_[ii],hh++;
	}
	Sort(arr,hh);
	who.Set(arr,hh);
	delete[]arr;
	return who;
}

void SimpleIntSet::Set(const int* values, int size){
	int i;
	for(i=0;i<size;i++) values_[i]=values[i];
	size_=size;
}


