// simple_calculator.cc
#include "simple_calculator.h"
	
void SimpleCalculator::Add(double val){
	value_ += val;
}
void SimpleCalculator::Subtract(double val){
	value_ -= val;
}
void SimpleCalculator::Multiply(double val){
	value_ *= val;
}
void SimpleCalculator::Divide(double val){
	value_ /= val;
}
