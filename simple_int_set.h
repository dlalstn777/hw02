// simple_int_set.h

#ifndef _SIMPLE_INT_SET_H_
#define _SIMPLE_INT_SET_H_
#define MAX_SIZE 100

class SimpleIntSet {
 public:
  SimpleIntSet() { size_ = 0; }

  SimpleIntSet Intersect(const SimpleIntSet& int_set) const;
  SimpleIntSet Union(const SimpleIntSet& int_set) const;
  SimpleIntSet Difference(const SimpleIntSet& int_set) const;

  void Set(const int* values, int size);

  const int* values() const { return values_; }
  int size() const { return size_; }

 private:
  //const int MAX_SIZE = 100;
  int values_[MAX_SIZE];
  int size_;
};

#endif  // _SIMPLE_INT_SET_H_
