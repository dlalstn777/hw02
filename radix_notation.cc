// radix_notation.cc

#include <string>
#include <iostream>

using namespace std;

// Implement this function.
string RadixNotation(unsigned int number, unsigned int radix)
{
	int alpa,i;
	char *arr = new char[36], h='a';
	string str="";
	alpa=number;
	for(i=0;i<36;i++){
		if(i<10) arr[i]='0'+i;
		if(i>9) arr[i]=h+i-10;
	}
	for(i=0;i<number;i++){
		str=arr[alpa%radix]+str,alpa=alpa/radix;
		if(alpa/radix==0) {
			str=arr[alpa%radix]+str;	
			break; }
	} 
	delete[]arr;
	return str;
}

int main() {
  while (true) {
    unsigned int number, radix;
    cin >> number >> radix;

		if (radix < 2 || radix > 36) break;
    cout << RadixNotation(number, radix) << endl;
  }



  return 0;
}
