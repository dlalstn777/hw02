// draw_points.cc

#include <iostream>
using namespace std;
struct Point {
  unsigned int x, y;
};
int **arrd;
int maxx=0,maxy=0,xx,yy;
// Implement this function.
void DrawPoints(const Point points, int num_points){
	int h,i,f,max;
	char **arr;
	if(maxx<=maxy) max=maxy;	
	if(maxy<maxx) max=maxx;

	arr=new char*[max+1];
	for(i=0;i<max+1;i++) arr[i]= new char[max+1];
	for(i=0;i<maxx+1;i++){
		for(f=0;f<maxy+1;f++) arr[i][f]='.';
	} 
	for(h=1;h<num_points+1;h++){
		xx=arrd[h][0],yy=arrd[0][h],arr[xx][yy]='*';	
	}
	for(i=0;i<maxx+1;i++){
		for(f=0;f<maxy+1;f++) cout<<arr[i][f];
		cout<<'\n';
	} 
	for(i=0;i<max+1;i++) delete[]arr[i];
	delete []arr;
}


int main() {
	while(true){
		int count,hi,t;
		Point cord;
		cin>>count;
		maxx=0,maxy=0;
		if(count==0) return 0;
		arrd=new int*[count+1];
		t=count;
		for(hi=0;hi<count+1;hi++) arrd[hi]=new int[count+1];
		while(t>0){
			cin>>cord.y>>cord.x;
			if(maxx<cord.x) maxx=cord.x;
			if(maxy<cord.y) maxy=cord.y;
			arrd[t][0]=cord.x,arrd[0][t]=cord.y;
			t--;
		}
		DrawPoints(cord,count);
		for(hi=0;hi<count+1;hi++) delete[]arrd[hi];
		delete []arrd;
	}
}
